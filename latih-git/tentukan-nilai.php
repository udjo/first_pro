<?php

function tentukan_nilai($string)
{

        if ($string > 80) {
            echo "Sangat Baik <br>";
        }else if ($string > 60) {
            echo "Baik <br>";
        }else if ($string > 50) {
            echo "Cukup <br>";
        }else if ($string > 40) {
            echo "Kurang <br>";
        }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>