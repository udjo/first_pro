<?php

// require_once('animal.php');
require('frog.php');
require('ape.php');

$sheep = new animal("shaun");
    echo "Nama Hewan : $sheep->name <br>";
    echo "Jumlah Kaki: $sheep->legs <br>";
    echo "Cold Blooded Animal: $sheep->cold_blooded <br><br>";

$katak = new frog("Buduk");
    echo "Nama Hewan : $katak->name <br>";
    // echo "Jumlah Kaki : $katak->legs <br>";
    // echo "Cold Blooded Animal: $katak->cold_blooded <br>";
    $katak->jump();
    echo "<br><br>";

$sungokong = new ape("Kera Sakti");
    echo "Nama Hewan : $sungokong->name <br>";
    // echo "Jumlah Kaki : $sungokong->legs <br>";
    // echo "Cold Blooded Animal: $sungokong->cold_blooded <br>";
    $sungokong->yell();
?>